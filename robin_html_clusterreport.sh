
#!/bin/bash
# Initialize variables

OUTPUT_DIR="/tmp/k8s-report"
DATE=$(date '+%Y-%m-%d %H:%M:%S')
LOG_FILE="$OUTPUT_DIR/report-$DATE.log"
REPORT_FILE="$OUTPUT_DIR/report-$DATE.html"
ruser=$1
rpassword=$2

# Create output directory
mkdir -p "$OUTPUT_DIR"

# Redirect all output to log file
exec &> >(tee -a "$LOG_FILE")

# Start generating HTML report
cat <<EOF > "$REPORT_FILE"
<!DOCTYPE html>
<html>
<head>
  <title> Robin CNP Cluster Report</title>
  <style>
    body {
      font-family: Arial, sans-serif;
    }
    h1 {
      margin-bottom: 10px;
    }
    table {
      border-collapse: collapse;
      margin-bottom: 20px;
    }
    th, td {
      padding: 8px;
      border: 1px solid #ddd;
    }
    th {
      background-color: #f2f2f2;
      text-align: left;
      font-weight: normal;
    }
  </style>
</head>
<body style="background-color:LightYellow;">
  <h1 style="color:blue">Robin Cloud Native Platform Cluster Report</h1>
  <p>Generated at $DATE and executed on $(hostname)</p>
  <nav> 
  <section id="nav">
    <ul>
      <li><a href="#hostInfo">Host Information</a></li>
        <ul> 
          <li><a href="#kernelInfo">Kernel Info</a></li>
          <li><a href="#osInfo">OS Info</a></li>
          <li><a href="#memInfo">Memory Info</a></li>
          <li><a href="#cpuInfo">CPU Info</a></li>
          <li><a href="#robinVersion">Robin Version</a></li>
          <li><a href="#dockerVersion">Docker Version</a></li>
        </ul> 
      <li><a href="#hostDiskSpace">Host Disk Space</a></li>
      <li><a href="#k8sHealth">Kubenetes Cluster Info </a></li>
        <ul> 
          <li><a href="#k8sNodeHealth">Nodes Health</a></li>
          <li><a href="#k8sPodHealth">Pods Health</a></li>
          <li><a href="#k8sDeployHealth">Deployments Health</a></li>
          <li><a href="#k8sSvcHealth">Services Health</a></li>
          <li><a href="#k8sPvcHealth">Persistent Volume Claims Health</a></li>
          <li><a href="#k8sKubeletHealth">Kubelet Service Status</a></li>
          <li><a href="#helmList">Helm Charts</a></li>
        </ul> 
      <li><a href="#robinClusterInfo">Robin Cluster Info</a></li>
       <ul> 
          <li><a href="#robinHostHealth">Robin Host Health</a></li>
        </ul> 
      <li><a href="#robinAppInfo">Robin Application Info</a></li>
      <ul> 
          <li><a href="#robinAppHealth">Robin App Health</a></li>
          <li><a href="#robinInstancesList">Robin Instance List</a></li>
          <li><a href="#robinCollectionList">Robin Collection List</a></li>
        </ul> 
      <li><a href="#robinServiceStatus">Robin Service Status</a></li>
      <li><a href="#robinStorageHealth">Robin Storage Health</a></li>
      <ul> 
          <li><a href="#robinDriveInfo">Robin Drive Health</a></li>
          <li><a href="#robinVolumeinfo">Robin Volume List</a></li>
        </ul> 
      <li><a href="#networkingHealth">Networking Status</a></li>
      <ul> 
          <li><a href="#k8sIppools">Kubernetes IP pools</a></li>
          <li><a href="#robinIppools">Robin IP pools</a></li>
        </ul> 
      <li><a href="#robinMetricsStatus">Robin Metrics Status</a></li>
      <li><a href="#robinJobList">Robin Job List</a></li>
      <li><a href="#robinlicenseStatus">Robin License Info</a></li>
      <li><a href="#robinConfigInfo">Robin Config Info</a></li>
    </ul>
    </section>
  </nav>
EOF


#Functions

# For Login
validate_login()
{
  robin login $ruser --password $rpassword

  if [ $? != 0 ]; then
      echo " " 
      echo " Oops!! something wrong,  please pass the correct robin login credentials "
      echo "  "
      echo "  example:  $0 <robin user> <robinpassword>"
      echo "exiting ...."
      exit 0
  fi

}

# List Host information 
host_info()
{
    echo "<section id="hostInfo">"  >> "$REPORT_FILE"
    echo "<h2 >Host Info</h2>"

    echo "<section id="kernelInfo">"  >> "$REPORT_FILE"
    echo "<p><b> Kernel info</b></p>"
        uname -a;
    echo "</section>  " >> "$REPORT_FILE"

    echo "<section id="osInfo">"  >> "$REPORT_FILE"
    echo "<p><b> OS Info</b></p>"
    cat /etc/redhat-release
     echo '\n'
     echo uptime: $(uptime);
     hostname -i
    echo "</section>  " >> "$REPORT_FILE"

    echo "<section id="memInfo">"  >> "$REPORT_FILE"
    echo "<p><b>Mem info</b></p>"
    cat /proc/meminfo|grep Mem
    echo "</section>  " >> "$REPORT_FILE"

    echo "<section id="cpuInfo">"  >> "$REPORT_FILE"
    echo "<p><b>CPU info</b></p>"
    nproc --all
    echo "</section>  " >> "$REPORT_FILE"

    echo "<section id="robinVersion">"  >> "$REPORT_FILE"
    echo "<p><b>Robin Version</b></p>"
    echo "<pre>"
    robin version
    echo "</pre>"
    echo "</section>  " >> "$REPORT_FILE"

    echo "<section id="dockerVersion">"  >> "$REPORT_FILE"
    echo "<p><b>Docker Version</b></p>"
    echo "<pre>"
    docker version 
    echo "</pre>"
    echo "</section>  " >> "$REPORT_FILE"


    echo "</section>  " >> "$REPORT_FILE"

    echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"
}

# Host Disk Space 
filesystem_space()
{
    echo "<section id="hostDiskSpace">"  >> "$REPORT_FILE"
    echo "<h2>Host Disk Space</h2>"
    echo "<h3>Filesystem space</h3>"
    echo "<pre>"
    df -h|grep -vE 'kubelet|plugins|containers'
    echo "</pre>"
    echo "</section>  " >> "$REPORT_FILE"

}


#List the K8s Nodes Health
k8snode_status()
{
  echo "<section id="k8sHealth">"  >> "$REPORT_FILE"

  # List nodes
  echo "<h2>Kubernetes Cluster Information</h2>" >> "$REPORT_FILE"

  echo "<section id="k8sNodeHealth">"  >> "$REPORT_FILE"
  echo "<h3>Nodes Health</h3>" >> "$REPORT_FILE"
  kubectl get nodes -o wide | awk -F ' {2,}' 'BEGIN{print "<table>"} NR==1{print "<thead><tr>";for(i=1;i<=NF;i++)print "<th>"$i"</th>";print "</tr></thead><tbody>"} NR>1{print "<tr>";for(i=1;i<=NF;i++)print "<td>"$i"</td>";print "</tr>"} END{print "</tbody></table>"}' >> "$REPORT_FILE"
  echo "</section>  " >> "$REPORT_FILE"

  echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"

  # List pods
  echo "<section id="k8sPodHealth">"  >> "$REPORT_FILE"
  echo "<h3>Pods Health</h3>" >> "$REPORT_FILE"
  kubectl get pods -o wide --all-namespaces | awk -F ' {2,}' 'NR==1{print "<table><tr>"; for(i=1;i<=NF;i++)print "<th>" $i "</th>"; print "</tr>"} NR>1{print "<tr>"; for(i=1;i<=NF;i++)print "<td>" $i "</td>"; print "</tr>"} END{print "</table>"}' >> "$REPORT_FILE"
  echo "</section>  " >> "$REPORT_FILE"

  echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"

  # List deployments
  echo "<section id="k8sDeployHealth">"  >> "$REPORT_FILE"
  echo "<h3>Deployments Health</h3>" >> "$REPORT_FILE"
  kubectl get deployments -o wide --all-namespaces | awk -F ' {2,}' 'NR==1{print "<table><tr>"; for(i=1;i<=NF;i++)print "<th>" $i "</th>"; print "</tr>"} NR>1{print "<tr>"; for(i=1;i<=NF;i++)print "<td>" $i "</td>"; print "</tr>"} END{print "</table>"}' >> "$REPORT_FILE"
  echo "</section>  " >> "$REPORT_FILE"

  echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"

  # List services
  echo "<section id="k8sSvcHealth">"  >> "$REPORT_FILE"
  echo "<h3>Services Health</h3>" >> "$REPORT_FILE"
  kubectl get services -o wide --all-namespaces | awk -F ' {2,}' 'NR==1{print "<table><tr>"; for(i=1;i<=NF;i++)print "<th>" $i "</th>"; print "</tr>"} NR>1{print "<tr>"; for(i=1;i<=NF;i++)print "<td>" $i "</td>"; print "</tr>"} END{print "</table>"}' >> "$REPORT_FILE"
  echo "</section>  " >> "$REPORT_FILE"

  echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"

  # List persistent volume claims
  echo "<section id="k8sPvcHealth">"  >> "$REPORT_FILE"
  echo "<h3>Persistent Volume Claims Health</h3>" >> "$REPORT_FILE"
  kubectl get pvc -o wide --all-namespaces | awk -F ' {2,}' 'NR==1{print "<table><tr>"; for(i=1;i<=NF;i++)print "<th>" $i "</th>"; print "</tr>"} NR>1{print "<tr>"; for(i=1;i<=NF;i++)print "<td>" $i "</td>"; print "</tr>"} END{print "</table>"}' >> "$REPORT_FILE"
  echo "</section>  " >> "$REPORT_FILE"

  echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"

  # Describe the status of kubelet service 
  echo "<section id="k8sKubeletHealth">"  >> "$REPORT_FILE"
  echo "<h3>Kubelet Service status</h3>" >> "$REPORT_FILE"
  kubelet_status=$(systemctl status kubelet)
  html_output="<html><body><pre>${kubelet_status}</pre></body></html>"
  echo "${html_output}"  >> "$REPORT_FILE"
  echo "</section>  " >> "$REPORT_FILE"

  echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"

  # List Helm charts
  echo "<section id="helmList">"  >> "$REPORT_FILE"
  echo "<h3>Helm Charts</h3>" >> "$REPORT_FILE"
  helm_list=$(helm list -A | tail -n +2)
  # Convert helm list output into HTML table
  html_table="<table border='1' cellpadding='5'>
  <thead><tr><th>NAME</th><th>NAMESPACE</th><th>REVISION</th><th>UPDATED</th><th>STATUS</th><th>CHART</th><th>APP VERSION</th></tr></thead>
  <tbody>"
  # Loop through each line of the helm list output and append it to the HTML table
  while read -r line; do
    html_table+="<tr><td>$(echo "$line" | awk '{print $1}')</td><td>$(echo "$line" | awk '{print $2}')</td><td>$(echo "$line" | awk '{print $3}')</td><td>$(echo "$line" | awk '{print $4" "$5" "$6" "$7}')  </td><td> $(echo "$line" | awk '{print $8}') </td><td>$(echo "$line" | awk '{print $9}')</td><td>$(echo "$line" | awk '{print $10}')</td></tr>"
  done <<< "$helm_list"
  html_table+="</tbody></table>"
  # Write HTML table to a file
  echo "${html_table}" >> "$REPORT_FILE"
  # helm list -A | awk -F ' {2,}' 'NR==1{print "<table><tr>"; for(i=1;i<=NF;i++)print "<th>" $i "</th>"; print "</tr>"} NR>1{print "<tr>"; for(i=1;i<=NF;i++)print "<td>" $i "</td>"; print "</tr>"} END{print "</table>"}' >> "$REPORT_FILE"
  # helm list -A -o json | jq -r '.[] | [.name, .namespace, .revision, .updated, .status, .chart, .app_version] | @tsv' | awk -v OFS='\t' '{print "<tr><td>"$1"</td><td>"$2"</td><td>"$3"</td><td>"$4"</td><td>"$5"</td><td>"$6"</td><td>"$7"</td></tr>"}' | sed '1s;^;<table><tr><th>Name</th><th>Namespace</th><th>Revision</th><th>Updated</th><th>Status</th><th>Chart</th><th>App Version</th></tr>;' | sed '$s;$;</table>;' >> "$REPORT_FILE"
  echo "</section>  " >> "$REPORT_FILE"

  echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"

  echo "</section>  " >> "$REPORT_FILE"
}


# Robin Health Status 
robin_host_health(){
  echo "<section id="robinClusterInfo">"  >> "$REPORT_FILE"
  echo "<h2>Robin Cluster Information</h2>" >> "$REPORT_FILE"
  
  echo "<section id="robinHostHealth">"  >> "$REPORT_FILE"
  echo "<h3>Robin Host Health</h3>" >> "$REPORT_FILE"
  echo "<pre  >"
  robin host list
  robin manager list
  echo "</pre>"

  echo "</section>"
    
  
  echo "</section>"

  echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"
}

robin_app_info(){
  echo "<section id="robinAppInfo">"  >> "$REPORT_FILE"
  echo "<h2>Robin Application Information</h2>" >> "$REPORT_FILE"

  # List Robin apps 
  echo "<section id="robinAppHealth">"  >> "$REPORT_FILE"
  robin_app=$(robin app list)
  table1=$(echo "$robin_app" | awk '/ROBIN Bundle Apps:/{flag=1;next}/Helm\/Flex Apps:/{flag=0}flag' | sed 's/+/-/g')
  # Extract the second table
  table2=$(echo "$robin_app" | awk '/Helm\/Flex Apps:/{flag=1;next}flag' | sed 's/+/-/g')
  echo "<h3>Robin App Health</h3>" >> "$REPORT_FILE"
  # Print the tables in HTML format
  echo "<h4>ROBIN Bundle Apps:</h4>" >> "$REPORT_FILE" 
  echo "$table1" | awk -F '|' 'BEGIN{printf "<table>"} {printf "<tr>"; for(i=2;i<=NF-1;i++) printf "<td>%s</td>", $i; printf "</tr>"} END{print "</table>"}' >> "$REPORT_FILE" 
  echo "<h4>Helm/Flex Apps:</h4>" >> "$REPORT_FILE" 
  echo "$table2" | awk -F '|' 'BEGIN{printf "<table>"} {printf "<tr>"; for(i=2;i<=NF-1;i++) printf "<td>%s</td>", $i; printf "</tr>"} END{print "</table>"}' >> "$REPORT_FILE" 
  # robin app list >> "$REPORT_FILE" 
  echo "</section>"

  echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"

  echo "<section id="robinInstancesList">"  >> "$REPORT_FILE"
  # List Robin Instances
  echo "<h3>Robin Instances List</h3>" >> "$REPORT_FILE"
  echo "<pre  >"
  robin instance list
  echo "</pre>"
  echo "</section>"

  # List Robin Collection 
  echo "<section id="robinCollectionList">"  >> "$REPORT_FILE"
  echo "<h3>Robin Collection List</h3>" >> "$REPORT_FILE"
  echo "<pre  >"
  robin collection list
  echo "</pre> " 
  echo "</section>"

  echo "</section>"

  echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"
}

robin_storage_health(){
  echo "<section id="robinStorageHealth">"  >> "$REPORT_FILE"
  echo "<h3>Robin Storage Health</h3>"
  # Robin Drive Health 


    echo "<section id="robinDriveInfo">"  >> "$REPORT_FILE"
    echo "<h3>Robin Drive Health</h3>"
    echo "<pre  >"
    # robin_drive=$(robin drive list)
    # echo "$robin_drive" | awk -F '|' 'BEGIN{printf "<table>"} {printf "<tr>"; for(i=2;i<=NF-1;i++) printf "<td>%s</td>", $i; printf "</tr>"} END{print "</table>"}' >> "$REPORT_FILE" 
    robin drive list
    echo "</pre>"
    echo "</section>"

  # Robin Volume Health 
   echo "<section id="robinVolumeinfo">"  >> "$REPORT_FILE"
    echo "<h3>Robin Volume Info</h3>"
    echo "<pre  >"
    robin volume list
    echo "</pre>"
    echo "</section>"

    echo "</section>"

    echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"

}



# Status of Robin Service  
robin_service_status()
{
    echo "<section id="robinServiceStatus">"  >> "$REPORT_FILE"
    echo "<h2>Robin Worker Services Status </h2>"
    echo "<pre  >"
    robin host list --services | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2};?)?)?[mGK]//g"
    robin manager list --services | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2};?)?)?[mGK]//g"
    echo "</pre>"
    echo "</section>"

    echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"
}



# Networking Health 
networking_status()
{
  echo "<section id="networkingHealth">"  >> "$REPORT_FILE"
  echo "<h2>Networking Status </h2>"

  # List Kubernest IP-Pools 
  echo "<section id="k8sIppools">"  >> "$REPORT_FILE"
  echo "<h3> Kubernetes ip-pools </h3>"
  echo "<pre  >"
  kubectl get ippools -A -o wide | awk -F ' {2,}' 'NR==1{print "<table><tr>"; for(i=1;i<=NF;i++)print "<th>" $i "</th>"; print "</tr>"} NR>1{print "<tr>"; for(i=1;i<=NF;i++)print "<td>" $i "</td>"; print "</tr>"} END{print "</table>"}' >> "$REPORT_FILE"
  echo "</pre>"
  echo "</section>"

  #List Robin Ip-pools 
  echo "<section id="robinIppools">"  >> "$REPORT_FILE"
  echo "<h3>Robin ip-pools  </h3>"
  echo "<pre  >"
  robin ip-pool list 
  echo "</pre>"
  echo "</section>"

  echo "</section>"

  echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"
}

# Robin Metric status 
robin_metric_status()
{
   echo "<section id="robinMetricsStatus">"  >> "$REPORT_FILE"
   echo "<h2>Robin Metric Status</h2>"
   echo "<pre  >"
   robin metrics status | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2};?)?)?[mGK]//g"
   echo "</pre>"
   echo "</section>"

   echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"
}

robin_job_info()
{
   echo "<section id="robinJobList">"  >> "$REPORT_FILE"
   echo "<h2>Robin Job List</h2>"
   echo "<pre  >"
   robin job list | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2};?)?)?[mGK]//g"
   echo "</pre>"
   echo "</section>"

   echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"
}

robin_license_status()
{
   echo "<section id="robinlicenseStatus">"  >> "$REPORT_FILE"
   echo "<h2>Robin License Status</h2>"
   echo "<pre  >"
   robin license info | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2};?)?)?[mGK]//g"
   echo "</pre>"
   echo "</section>"

   echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"
}


robin_config_info()
{
   echo "<section id="robinConfigInfo">"  >> "$REPORT_FILE"
   echo "<h2>Robin Config Info</h2>"
   echo "<pre  >"
   robin config list | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2};?)?)?[mGK]//g"
   echo "</pre>"
   echo "</section>"

   echo "<a href="#nav"> Go to Navigation </a>" >> "$REPORT_FILE"
}


validate_login
host_info >> "$REPORT_FILE"
filesystem_space >> "$REPORT_FILE"
k8snode_status >> "$REPORT_FILE"
robin_host_health >> "$REPORT_FILE"
robin_app_info >> "$REPORT_FILE"
robin_storage_health >> "$REPORT_FILE"
robin_service_status >> "$REPORT_FILE" 
networking_status >> "$REPORT_FILE" 
robin_metric_status >> "$REPORT_FILE"
robin_job_info >> "$REPORT_FILE"
robin_license_status >> "$REPORT_FILE"
robin_config_info >> "$REPORT_FILE"

